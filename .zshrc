# Path to your oh-my-zsh installation.
# export ZSH=/usr/share/oh-my-zsh

# Add doom files to PATH
#export PATH="$HOME/.emacs.d/bin:$PATH"

# Add haskell executables for editing
#export PATH="$HOME/.local/bin:$HOME/.ghcup/bin:$HOME/.cabal/bin:$PATH"

# Set theme to load
#ZSH_THEME="ys"

# Completion settings
#CASE_SENSITIVE="false"
#HYPHEN_INSENSITIVE="true"

# Plugin settings
#plugins=(
#    git 
#    extract
#    colored-man-pages
#    )

# Source Oh-My-Zsh
#source $ZSH/oh-my-zsh.sh

# Set Vi keybindings
#bindkey -v

# Source dotfiles
#source ~/.aliases

# Set colours (based on background)
#(cat ~/.cache/wal/sequences &)
#source ~/.cache/wal/colors-tty.sh  # tty support

# dircolors
#if [ -x "$(command -v dircolors)" ]; then
#  eval "$(dircolors -b ~/.dircolors)"
#fi

# For better font in urxvt, use:
# urxvt -fn "xft:Fira Code:style=Regular"
# Or set the font value in .Xresources to this.
# Then run xrdb -merge  ~/.Xresources

# Better history management
#bindkey "^[[A" history-beginning-search-backward
#bindkey "^[[B" history-beginning-search-forward

# Syntax highlighting
# install zsh-syntax-highlighting first
#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Setup 'thefuck'
#eval $(thefuck --alias)

# Add default ledger file for hledger
#export LEDGER_FILE="/home/chuckles/Documents/Personal/Finance/2022-2023.journal"

### Extra auto-completion
# Stack
#autoload -U +X bashcompinit && bashcompinit
#eval "$(stack --bash-completion-script stack)" 

#[ -f "/home/chuckles/.ghcup/env" ] && source "/home/chuckles/.ghcup/env" # ghcup-env


#export PATH="$PATH:$HOME/.spicetify"

# fish
