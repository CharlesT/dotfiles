" Tips from: 
"           https://www.shortcutfoo.com/blog/top-50-vim-configuration-options/
"           http://vim.wikia.com/wiki/Indenting_source_code

" Init plugins
" execute pathogen#infect()

:set clipboard=unnamedplus " allow clipboard access to and from X
syntax on " Coloured syntax highlighting for most files.

" Indentation settings
set autoindent " Lines inherit indentation of previous lines
set expandtab " Tabs to spaces
filetype indent on " File-specific rules
set expandtab " Enable use of soft tab stop number of spaces
set shiftwidth=2 " Help with auto indentation
set softtabstop=2 " Change tabs to 2 spaces
set smarttab " Insert the above number of spaces when tab is pressed.

" Search settings
set hlsearch " Enable search highlighting

" Text rendering options
set wrap " Line wrapping
set scrolloff=3 " Keeps 3 lines above and below cursor at all times when scrolling
set linebreak " Don't wrap a line in the middle of a word

" User Interface
set number " Show line numbers
set mouse=a " Enable mouse use for scrolling + resizing
set title " Show the file name

" Miscellaneous
" set spell " Enable spell-checking  -- Adds weird highlighting...
set history=1000 " Increase undo limit

" Hdevtools keybindings
au FileType haskell nnoremap <buffer> <F1> :HdevtoolsType<CR>
au FileType haskell nnoremap <buffer> <silent> <F2> :HdevtoolsInfo<CR>
au FileType haskell nnoremap <buffer> <silent> <F3> :HdevtoolsClear<CR>

" Setup pathogen (package installer)
execute pathogen#infect()

" Setup hledger-vim
" set runtimepath+=$HOME/.vim/hledger-vim
" autocmd FileType hledger setlocal omnifunc=hledger#complete#omnifunc


