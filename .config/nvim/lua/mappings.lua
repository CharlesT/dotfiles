require "nvchad.mappings"
local neogit = require "neogit"

-- add yours here

local map = vim.keymap.set

-- map("n", ";", ":", { desc = "CMD enter command mode" })
map("i", "jk", "<ESC>")

-- Comment (use spc-; instead of spc-/)
map("n", "<leader>;", "gcc", { desc = "toggle comment", remap = true })
map("v", "<leader>;", "gc", { desc = "toggle comment", remap = true })

-- Git
vim.keymap.set("n", "<leader>gg", neogit.open, {})
-- map({ "n", "i", "v" }, "<C-s>", "<cmd> w <cr>")
