#!/usr/bin/env bash

# import pywal colors
source "$HOME/.cache/wal/colors.sh"

swaylock \
  -d \
  -f \
  --image "$(< "${HOME}/.cache/wal/wal")" \
  --fade-in 1 \
  --scaling fill \
  \ #--effect-blur 10x10 \
  \
  --indicator-radius 100 \
  --indicator-thickness 10 \
  --inside-color 00000000 \
  --inside-clear-color 00000000 \
  --inside-ver-color 00000000 \
  --inside-wrong-color 00000000 \
  --key-hl-color "$color6" \
  --bs-hl-color "$color2" \
  --ring-color "$background" \
  --ring-clear-color "$color2" \
  --ring-wrong-color "$color1" \
  --ring-ver-color "$color3" \
  --line-uses-ring \
  --line-color 00000000 \
  --text-color 00000000 \
  --text-clear-color "$color2" \
  --text-wrong-color "$color1" \
  --text-ver-color "$color4" \
  --separator-color 00000000

