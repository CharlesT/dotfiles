if status is-interactive
    # Commands to run in interactive sessions can go here
    set fish_greeting ""
end

thefuck --alias | source
source ~/.aliases

export LEDGER_FILE="~/Documents/Personal/Finance/2022.beancount"

# Add doom & haskell executables to path
export PATH="$HOME/.config/emacs/bin:$PATH"
export PATH="$HOME/.local/bin:$HOME/.ghcup/bin:$HOME/.cabal/bin:$PATH"
# Add rust executables to path
export PATH="$HOME/.cargo/bin:$PATH"

# Vi keybindings
fish_vi_key_bindings

# Pyenv setup
pyenv init - | source

# Setup autojump for better directory navigation
[ -f /usr/share/autojump/autojump.fish ]; and source /usr/share/autojump/autojump.fish

# GENERAL KEYBINDS

if status --is-interactive
    # source $HOME/.config/fish/interactive.fish
    function fish_user_key_bindings
        bind \f 'complete'
    end
end

# Nix

export NIX_REMOTE=daemon

# IHP direnv
direnv hook fish | source
set -g direnv_fish_mode eval_on_arrow    # trigger direnv at prompt, and on every arrow-based directory change (default)
# set -g direnv_fish_mode eval_after_arrow # trigger direnv at prompt, and only after arrow-based directory changes before executing command
# set -g direnv_fish_mode disable_arrow    # trigger direnv at prompt only, this is similar functionality to the original behavior
